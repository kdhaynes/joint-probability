###############################################################################
Calculate Joint Probability Using a Bayesian Network
###############################################################################

Project includes two directories:
- docs: Contains project description.
- python: Contains the program (joint_prob.py),
                       input files (network.txt and query.txt), and
		       numerous alternative queries (alt_tests)


Program joint_prob.py calculates the joint probability for a given
Bayesian network.  The program uses Python (version 3.3), with the standard
packages sys and time.

INPUT:
network.txt: The bayesian network for n variables.
query.txt: The query consisting of the values of the n variables.

OUTPUT:
The joint probability as a single number with exactly 6 digits after the
decimal point.
(Note: There will be a newline at the end, just like the example output we
were provided.)

RUN:
>python joint_prob.py


###############################################################################
Katherine Haynes
CS440 Assignment 6
2019/05/10