#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May  4 14:10:13 2019

@author: kdhaynes
"""

import sys, time
t0=time.time()

fquery = 'query.txt'
fnetwork = 'network.txt'
ptime = False

# ========================================
def find_probability(fquery,fnetwork):

    probability = 1.0
    
    with open(fquery) as f1:
        qline = f1.readline().rstrip()
    qvals = qline.split()
    nvals = len(qvals)

    with open(fnetwork) as f2:
        nquery = int(f2.readline().rstrip())
        
        if (nvals != nquery):
            print("Expecting same number of query values")
            print("   as network size!!")
            return probability
            
        for i in range(nquery):
            line = list(map(int,(f2.readline().rstrip()).split()))
            nltoread = 2**line[0]
            nparents = line[0]
            
            nodeval = qvals[line[1]-1]
            parents = [None]*nparents
            for p in range(nparents):
                parents[p] = qvals[int(line[2+p]-1)]
                
            for j in range(nltoread):
                pline = (f2.readline().rstrip()).split()
                
                match = True
                for p in range(nparents):
                    if (pline[p] != parents[p]):
                        match = False
                
                if (match):
                    newprob = float(pline[nparents])
                    if (nodeval == 'F'):
                        newprob = 1. - newprob
                    probability *= newprob                       
            
    return probability

# ========================================
qprob =  find_probability(fquery,fnetwork)
print('{:.6f}'.format(qprob))

# Print out time information
if (ptime):
    timetaken = time.time() - t0
    print("")
    print("Time Taken (s): ",'{:5f}'.format(timetaken))
